jQuery(function(){
	var _d = jQuery(document);
	var _body = jQuery('#body');
	var _dropdown = jQuery('.dropdown');
	var _userbox = jQuery('.userbox > a');
	var _category = jQuery('.category');
	var _categoryItem = jQuery('.callSubCategory');
	var _categoryBox = jQuery('.subcategory-section');
	var _categoryWrap = jQuery('.subcategory-wrap');
	var _sf = jQuery('.superfish');
	var _sfItem = jQuery('.superfish li');
	var _sfNow = parseInt(_sf.data('category'), 10);
	var _pinTop = jQuery('.pin-top');
	var _notice = jQuery('.notice');

	_d.on('click', '.closeTips', function(){
        jQuery('.overlayer, .closeTips, .tips-login').remove();
    });

	_d.on('click', '.callLogin', function(){
		jQuery.callLogin();
	});

	_d.on('click', '.notice > a.close', function(){
		var that = jQuery(this);

		that.parent('.notice').fadeOut('fast', function(){
			jQuery(this).remove();

			if(jQuery('.notice').length == 0){
				jQuery('#system-message').remove();
			}
		});
	});

	_dropdown.each(function(i){
		var that = jQuery(this);
		that.on('click', function(){
			if(!that.hasClass('active')){
				that.addClass('active');
			}else{
				that.removeClass('active');
			}
		});

		that.bind("clickoutside", function(event){
			that.removeClass('active');
		});
	});

	_userbox.click(function(){
		var that = jQuery(this);
		var child = that.next('.userbox-dropdown');

		if(!child.hasClass('active')){
			child.addClass('active');
		}else{
			child.removeClass('active');
		}
	});

	_userbox.bind("clickoutside", function(event){
		jQuery(this).next('.userbox-dropdown').removeClass('active');
	});

	_categoryItem.each(function(i){
		var that = jQuery(this);

		that.on('mouseover', function(){
			_categoryItem.parent('li').removeClass('active');
			that.parent('li').addClass('active');
		});

		that.on('mouseout', function(){
			_categoryItem.parent('li').removeClass('active');
			_categoryItem.eq(_sfNow).parent('li').addClass('active');
		});
	});

	jQuery('.panel-close').on('click', function(){
		var _li = jQuery('.category li');
		_li.removeClass('active');

		jQuery(this).parent().fadeOut('fast');

		var whichCat = parseInt(_body.attr('data-category'), 10);

		if((typeof(_body.attr('data-category')) != 'underfined') && (whichCat>=0)){
			_li.eq(whichCat).addClass('active');
		}
	});

	jQuery(window).scroll(function(){
		var ws = jQuery(this).scrollTop();

		if(ws>0){
			_pinTop.addClass('active');
		}else {
			_pinTop.removeClass('active');
		}
	});

	_pinTop.on('click', function(){
		jQuery('html, body').stop().animate({scrollTop: 0});
	});

	/*
	jQuery('.pin-guest').on('click', function(){
		var href = jQuery(this).attr('data-href');

		_body.append('<div class="turnEx turnOut"><div class="loading"><div class="loading-outter"></div><div class="loading-inner"></div><div class="loading-text">Loading</div></div></div>');
		jQuery('.turnOut').animate({
			opacity: 1
		}, 500, function(){
			window.location.href = href;
		})
	});
	*/

	jQuery.extend({
		/*
		turnIn: function(){
			var me = jQuery('.turnIn');

			me.stop().animate({
				opacity: 0
			}, 500, function(){
				me.remove();
			});
		},
		*/
		fullBanner: function(){
			var banner = jQuery('.banner');
			var btns = jQuery('.banner-switch li');
			var allItem = jQuery('.banner-list li');

			btns.each(function(i){
				var that = jQuery(this);

				that.on('click', function(){
					btns.removeClass('active');
					that.addClass('active');
					allItem.removeClass('active');
					allItem.eq(i).addClass('active');
					banner.attr('data-now', i);
				});
			});
		},
		fullBannerTurner: function(){
			var banner = jQuery('.banner');
			var btns = jQuery('.banner-switch li');
			var allItem = jQuery('.banner-list li');
			var now = parseInt(banner.attr('data-now'), 10);
			var leng = allItem.length;

			now++;

			if(now>=leng){
				now = 0;
			}else if(now < 0){
				now = (leng-1);
			}

			btns.removeClass('active');
			btns.eq(now).addClass('active');
			allItem.removeClass('active');
			allItem.eq(now).addClass('active');

			banner.attr('data-now', now);
		},
		calcEarns: function(){
			var earns = jQuery('.calculated_earn');
			var target = jQuery('#sum_calculated_earn');
			var sum = 0;

			for(var i=0; i<earns.length; i++){
				var earnValue = parseInt(earns.eq(i).data('earn'), 10);
				sum += earnValue;
			}

			target.text(sum);
		},
		callLogin: function(){
			_body.append('<div class="overlayer"/><a class="close closeTips">&times;</a><div class="tips-login"><h4>會員登入</h4><p>登入會員身份後，才能使用此功能與服務。</p><div><a><img src="img/login-big.png" /></a></div></div>');
			jQuery('.overlayer, .closeTips, .tips-login').fadeIn('fast');
		},
		bringerSort: function(who, sort){
			var who = parseInt(who, 10);
			var btn = jQuery('.sort .btn');

			btn.removeClass('active color-danger');
			btn.eq(who).addClass('active color-danger');
		},
		panelToggle: function(){
			var _panel = $('.panel');
			var _toggle = _panel.children('.toggle');
			var _box = _panel.children('.panel-box');

			_toggle.each(function(i){
				var that = $(this);

				that.on('click', function(){
					if(that.hasClass('active')){
						that.removeClass('active');
						_box.addClass('active');
					}else {
						that.addClass('active');
						_box.removeClass('active');
					}
				});
			});
		},
		buyerCalc: function(){
			var _original = jQuery('#amount_note_original');
			var _originalPrice = parseInt(_original.text(), 10);
			var _shipping = jQuery('#amount_note_shipping');
			var _shippingPrice = parseInt(_shipping.val(), 10);
			var _tax = jQuery('#amount_note_tax');
			var _earn = jQuery('#amount_note_earn');
			var _sum = jQuery('#amount_note_sum');
			var _taxPercent = jQuery('#tax_percent');
			var _taxPercentValue = parseInt(_taxPercent.val(), 10)/100;
			var _earnPercent = jQuery('#earn_percent');
			var _earnPercentValue = parseInt(_earnPercent.val(), 10)/100;
			var _jInput = jQuery('#jform_orderitem_amount');

			_tax.text(Math.floor(_originalPrice * _taxPercentValue));
			_earn.text(Math.floor(_originalPrice * _earnPercentValue));

			var cost = _originalPrice + parseInt(_tax.text(),10) + parseInt(_earn.text(),10) + _shippingPrice;
			_sum.text(cost);
			_jInput.val(cost);

			_shipping.on('keydown', function(){
				var that = jQuery(this);
				var newShipping = parseInt(that.val(), 10);

				_sum.text(_originalPrice + parseInt(_tax.text(),10) + parseInt(_earn.text(),10) + newShipping);
			});

			_taxPercent.change(function(){
				var that = jQuery(this);
				_tax.text(Math.floor(_originalPrice * (parseInt(that.val(), 10)/100)));
			});
		}
	});
});

jQuery(window).load(function(){
	var _sf = jQuery('.superfish');
	var _sfItem = jQuery('.superfish li');
	var _sfNow = parseInt(_sf.data('category'), 10);

	jQuery.bringerSort(1, 'asc');

	jQuery('.sort .btn').each(function(i){
		jQuery(this).click(function(){
			jQuery('.sort .btn').removeClass('active color-danger');
			jQuery(this).addClass('active color-danger');
		});
	});

	_sfItem.removeClass('active');
	_sfItem.eq(_sfNow).addClass('active');

	_sf.superfish({speed: 'fast'});

	jQuery('.nav-member').height('auto');
});